// Visual Micro is in vMicro>General>Tutorial Mode
// 
/*
    Name:       SonoGib.ino
    Created:  02/07/2019 17:10:13
    Author:     DESKTOP-7BUQ9V8\Gregor
*/

// Define User Types below here or use a .h file
//

#include "Notes.h"
#include <avr/io.h>

#define Buzzer 10
#define Key1 0x01
#define Key2 0x02
#define Key3 0x04
#define Key4 0x08
#define Key5 0x10
#define Key6 0x20
#define Key7 0x40
#define Key8 0x80

unsigned long duration=50;

unsigned long startMillis;
unsigned long currentMillis;
const unsigned long period = 10;  //the value is a number of milliseconds

#define KBUF_SIZE 16
char keybuffer[KBUF_SIZE];
char kb_in=0;
char kb_out=0;
char kb_num=0;

// Define Function Prototypes that use User Types below here or use a .h file
//

void ReadKBD();
char GetKey();
void Storekey(char key);


// The setup() function runs once each time the micro-controller starts
void setup()
{
  //Switches
  pinMode(8, INPUT_PULLUP);    // sets the digital pin 0 as Input with Pull-Up
  pinMode(9, INPUT_PULLUP);    // sets the digital pin 0 as Input with Pull-Up
  pinMode(2, INPUT_PULLUP);    // sets the digital pin 0 as Input with Pull-Up
  pinMode(3, INPUT_PULLUP);    // sets the digital pin 0 as Input with Pull-Up
  pinMode(4, INPUT_PULLUP);    // sets the digital pin 0 as Input with Pull-Up
  pinMode(5, INPUT_PULLUP);    // sets the digital pin 0 as Input with Pull-Up
  pinMode(6, INPUT_PULLUP);    // sets the digital pin 0 as Input with Pull-Up
  pinMode(7, INPUT_PULLUP);    // sets the digital pin 0 as Input with Pull-Up
  //Buzzer
  pinMode(11, OUTPUT);    // sets the digital pin 0 as Input with Pull-Up
  startMillis = millis();  //initial start time
}

// Add the main program code into the continuous loop() function
void loop()
{
  char cmd;
  currentMillis = millis();  //get the current time
  
  if (currentMillis - startMillis >= period)  // 10 ms has elapsed
  {
    startMillis = currentMillis;
    duration=analogRead(4)*2;
    ReadKBD();
    if (cmd=GetKey())
    {
      switch (cmd)
      {
        case 1: tone(Buzzer, NOTE_C4, duration); break;
        case 2: tone(Buzzer, NOTE_D4, duration); break;
        case 3: tone(Buzzer, NOTE_E4, duration); break;
        case 4: tone(Buzzer, NOTE_F4, duration); break;
        case 5: tone(Buzzer, NOTE_G4, duration); break;
        case 6: tone(Buzzer, NOTE_A4, duration); break;
        case 7: tone(Buzzer, NOTE_B4, duration); break;
        case 8: tone(Buzzer, NOTE_C5, duration); break;
        default: break;
      }
    }
  }
}

// Define Functions below here or use other .ino or cpp files
//

void ReadKBD()
{
  static char old;
  static char oldB;
  char nov=PIND;
  char novB=PINB;               
  char pressed=(nov^old)&old;
  char pressedB=(novB^oldB)&oldB;
  if (pressedB & Key1) Storekey(1);
  if (pressedB & Key2) Storekey(2);
  if (pressed & Key3) Storekey(3);
  if (pressed & Key4) Storekey(4);
  if (pressed & Key5) Storekey(5);
  if (pressed & Key6) Storekey(6);
  if (pressed & Key7) Storekey(7);
  if (pressed & Key8) Storekey(8);
  old=nov;
}

void Storekey(char key)
{
  if(kb_num<KBUF_SIZE)
  {
    keybuffer[kb_in]=key;
    kb_in++;
    if (kb_in >= KBUF_SIZE)
    {
      kb_in=0;
    }
    kb_num++;
  }
}

char GetKey()
{
  char key=0;
  if (kb_num>0)
  {
    key=keybuffer[kb_out];
    kb_out++;
    if (kb_out >= KBUF_SIZE) kb_out=0;
    kb_num--;
  }
  return key;
}
